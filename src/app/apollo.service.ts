import { Injectable } from '@angular/core';
import {Apollo, ApolloBase} from "apollo-angular";
import {Observable} from "rxjs";
import {ApolloQueryResult} from "@apollo/client";

@Injectable({
  providedIn: 'root'
})
export class ApolloService {

  private apollo: ApolloBase;

  constructor(private apolloProvider: Apollo) {
    this.apollo = this.apolloProvider.use('consumer');
  }

  getMenuList = ():Observable<ApolloQueryResult<any>> => {
   return this.apollo.watchQuery<any>({
      query: gql`{
          menu {
              title
              orderUI
              url
           }
       }`,
    })
  }
}
