import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import {BreakpointObserver} from "@angular/cdk/layout";
import {Apollo} from "apollo-angular";
import {gql} from "apollo-angular-boost/index";
import {Subscription} from "rxjs";
import {ApolloService} from "./apollo.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'julie';

  @ViewChild(MatSidenav) sidenav!: MatSidenav;
  subscriptions: Subscription = new Subscription();
  menuList = [];


  constructor(private observer: BreakpointObserver,
              private apolloService: ApolloService) {}

  ngAfterViewInit = () => {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }

  getMenuList = () => {
    this.subscriptions.add(
      this.apolloService.getMenuList().subscribe((resData) => {
        this.menuList = resData;
      }, error => {
        this.menuList = [
          {
            title: 'home 1',
            orderUI: 1,
            url: '/home'
          },
          {
            title: 'home 2',
            orderUI: 2,
            url: '/home'
          },
          {
            title: 'home 3',
            orderUI: 3,
            url: '/home'
          },
          {
            title: 'home 4',
            orderUI: 4,
            url: '/home'
          },{
            title: 'home 5',
            orderUI: 5,
            url: '/home'
          },{
            title: 'home 6',
            orderUI: 6,
            url: '/home'
          },{
            title: 'home 7',
            orderUI: 7,
            url: '/home'
          }
        ]
      })
    )
  }


}
